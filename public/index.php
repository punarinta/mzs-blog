<?php

chdir(__DIR__);
date_default_timezone_set('Europe/Stockholm');
require '../core/Article.php';

// read article DB
$articleList = array_map('str_getcsv', file('../db/articles/list'));
array_shift($articleList);

// dispatch URL
$uri = rtrim($_SERVER['REQUEST_URI'], '\\');
$uri = explode('?', $uri);
$loc = explode('/', ltrim($uri[0], '/'));

$translit = false;
$showAll = false;
$article = null;

switch ($loc[0])
{
    case 'cyrillic':
        // cyrillics news for now are shown all, not only 10
        $translit = true;

    case 'all':
        $showAll = true;

    case '':
        // render main page
        $content = '<ul id="toc">';

        if (!$showAll)
        {
            $articleList = array_slice($articleList, 0, 9);
        }

        foreach ($articleList as $item)
        {
            $content .= '<li class="toc-item"><a href="/' . $item[0];
            $content .= '"><span class="mzs-tlit">' . ($translit ? Article::tlit($item[1]) : $item[1]);
//            $content .= '</span><span class="toc-item-date">' . date('d.m.Y', strtotime($item[2])) . '</span></a></li>';
            $content .= '</span></a></li>';
        }
        $content .= '</ul>';
        $content .= '<a class="mzs-tlit" href="/all">Vsi izvěstija</a> ' . ($translit ? '': '| <a class="mzs-tlit" href="/cyrillic">Kirilicejų</a>');
        $title = 'Glavna stranica';
        break;

    case 'sync':
        $title = 'Article sync';
        require '../core/Sys.php';
        $content = Sys::exec('git pull 2>&1');
        break;

    default:
        $article = new Article($articleList, $loc[0]);
        if ($article->entry)
        {
            $content = $article->getContent();
            $title = $article->getTitle();
        }
        else
        {
            $content = '<p class="mzs-tlit">Izvini, članek ne byl najdeny. ☹</p>';
            $title = 'Greška 404';
            http_response_code(404);
        }
}

?>
<!DOCTYPE html>
<html lang="ns">
<head>
    <meta charset="utf-8">
    <title><?= $title ?> | Slovo</title>
    <meta name="keywords" content="<?= Article::isOK($article) ? implode(', ', $article->entry[3]) : 'blog' ?>,medžuslovjanski,interslavic">
    <meta name="description" content="<?= Article::isOK($article) ? $article->getAbstract() : 'News and blog on Medžuslovjanski (Interslavic) language.' ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600" rel="stylesheet" type="text/css">
    <link href="/css/index.css" rel="stylesheet" type="text/css">
</head>
<body>
<header>
    <?php if ($loc[0] != '') { ?>
        <button class="mzs-tlit" onclick="location='/'">Do glavnoj</button>
    <?php } ?>
    <?php if ($loc[0] != 'dobrodosli') { ?>
        <button class="mzs-tlit" onclick="location='/dobrodosli'">Čto to jest?</button>
    <?php } ?>
    <button onclick="mzs_tlit(1);this.remove()">ž → ж</button>
</header>
<article>
    <h1><?= $title ?></h1>
    <?= $content ?>
</article>
<?php if (Article::isOK($article)) { ?>
    <footer>
        <div class="mzs-tlit">Napisane v <?= $article->getDate() ?></div>
        <?php if ($tags = $article->getTagsString()) { ?>
        <div class="mzs-tlit">Nalěpki: <span class="tag"><?= $tags ?></span>.</div>
        <?php } ?>
        <div id="disqus_thread"></div>
        <script type="text/javascript">
            var disqus_shortname = 'slovo-news';
            (function() {
                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
    </footer>
<?php } ?>
</body>
<script>
    var i, el = document.querySelectorAll('title, h1');
    for (i = 0; i < el.length; i++) el[i].className = el[i].className + ' mzs-tlit';
</script>
<script src="/js/mzs-tlit.js"></script>
<script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter32755005 = new Ya.Metrika({ id:32755005, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/32755005" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
</html>