<?php

chdir(__DIR__ . '/..');
date_default_timezone_set('Europe/Stockholm');
require '../core/Article.php';

// read article DB
$articleList = array_map('str_getcsv', file('../db/articles/list'));
array_shift($articleList);

$corpus = new Corpus;

foreach ($articleList as $item)
{
    $article = new Article($articleList, $item[0]);
    $corpus->add($article->getTitle());
    $corpus->add($article->getContent());
}

$corpus->out();

class Corpus
{
    public $data = [];
    public $alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','r','s','t','u','v','y','z','č','ě','ę','ž','š','ų','ď','ľ','ń','ť'];

    public function add($str)
    {
        $syms = preg_split('/(?!^)(?=.)/u', $str);

        foreach ($syms as $sym)
        {
            $sym = strtolower(trim($sym));
            if (!in_array($sym, $this->alphabet))
            {
                continue;
            }

            if (!isset ($this->data[$sym]))
            {
                $this->data[$sym] = 1;
            }
            else
            {
                $this->data[$sym]++;
            }
        }
    }

    public function out()
    {
        asort($this->data);
        $this->data = array_reverse($this->data);
        print_r($this->data);
    }
}



