<?php

chdir(__DIR__);
date_default_timezone_set('Europe/Stockholm');

// read article DB
$articleList = array_map('str_getcsv', file('../db/articles/list'));
array_shift($articleList);

$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";

foreach ($articleList as $item)
{
    $xml .= "\t<url>\n";
    $xml .= "\t\t<loc>http://slovo.news/" . $item[0] . '</loc>' . "\n";
    $xml .= "\t\t<lastmod>" . date('Y-m-d', strtotime($item[2])) . '</lastmod>' . "\n";
    $xml .= "\t</url>\n";
}

$xml .= '</urlset>';

header('Content-Type: application/xml');
echo $xml;
