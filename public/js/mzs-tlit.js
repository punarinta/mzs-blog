function mzs_tlit(skipCheck)
{
  // check if we need to transliterate at all
  var lang = navigator.language.split('-')[0];

  if (typeof skipCheck == 'undefined' && ['ru', 'uk', 'be', 'bg', 'mk', 'sr'].indexOf(lang) == -1) return;

  var jot = (['ru', 'uk', 'be'].indexOf(lang) == -1) ? ['j','J'] : ['й','Й'];

  var map =
  {
    'a': 'а', 'A': 'А',
    'b': 'б', 'B': 'Б',
    'c': 'ц', 'C': 'Ц',
    'č': 'ч', 'Č': 'Ч',
    'd': 'д', 'D': 'Д',
    'e': 'е', 'E': 'Е',
    'ě': 'е', 'Ě': 'Е',
    'ę': 'я', 'Ę': 'Я',
    'f': 'ф', 'F': 'Ф',
    'g': 'г', 'G': 'Г',
    'h': 'х', 'H': 'Х',
    'i': 'и', 'I': 'И',
    'j': jot[0], 'J': jot[1],
    'k': 'к', 'K': 'К',
    'l': 'л', 'L': 'Л',
    'm': 'м', 'M': 'М',
    'n': 'н', 'N': 'Н',
    'o': 'о', 'O': 'О',
    'p': 'п', 'P': 'П',
    'r': 'р', 'R': 'Р',
    's': 'с', 'S': 'С',
    'š': 'ш', 'Š': 'Ш',
    't': 'т', 'T': 'Т',
    'u': 'у', 'U': 'У',
    'ų': 'у', 'Ų': 'У',
    'v': 'в', 'V': 'В',
    'y': 'ы', 'Y': 'Ы',
    'z': 'з', 'Z': 'З',
    'ž': 'ж', 'Ž': 'Ж'
  };

  var hardCons = ['б','в','г','д','з','к','л','н','п','р','с','т','ф','х'];

  // find all elements to proceed
  var i, j, el = document.querySelectorAll('.mzs-tlit');
  for (i = 0; i < el.length; i++)
  {
    var sym = el[i].innerText.split('');

    for (j = 0; j < sym.length; j++)
    {
      if (map[sym[j]]) sym[j] = map[sym[j]];
      if (sym[j-1] == jot[0] && sym[j] == 'у') { sym[j-1] = 'ю'; sym[j] = ''; }
      if (sym[j-1] == jot[0] && sym[j] == 'е') { sym[j-1] = 'е'; sym[j] = ''; }
      if (sym[j-1] == jot[0] && sym[j] == 'а') { sym[j-1] = 'я'; sym[j] = ''; }
      if (sym[j-1] == jot[0] && sym[j] == 'я') { sym[j-1] = 'я'; sym[j] = ''; }
      if (sym[j-1] == 'ш' && sym[j] == 'ч') { sym[j-1] = 'щ'; sym[j] = ''; }
      if (sym[j-1] == 'ť') { sym[j-1] = 'т'; sym.splice(j, 0, 'ь'); }
      if (sym[j-1] == 'ď') { sym[j-1] = 'д'; sym.splice(j, 0, 'ь'); }
      if (sym[j-1] == 'ń') { sym[j-1] = 'н'; sym.splice(j, 0, 'ь'); }
      if (sym[j-1] == 'ľ') { sym[j-1] = 'л'; sym.splice(j, 0, 'ь'); }
      if (sym[j-1] && hardCons.indexOf(sym[j-1].toLowerCase()) != -1 && sym[j] == jot[0]) { sym[j] = 'ь'; }
      if (sym[j-1] == 'ь' && sym[j] == 'е') { sym[j-1] = 'е'; sym[j] = ''; }
    }

    el[i].innerText = sym.join('');
  }
}

// mzs_tlit();