<?php

class Article
{
    public $entry = null;
    public $content = null;

    public function __construct($articleList, $url)
    {
        foreach ($articleList as $elem)
        {
            if ($elem[0] == $url)
            {
                $this->entry = $elem;
                $this->entry[3] = array_map('trim', explode(',', $this->entry[3]));
                $this->content = file_get_contents('../db/articles/' . $this->getDate('Y-m-d-') . $this->entry[0] . '/text.html');
                break;
            }
        }
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getAbstract()
    {
        return trim(implode(' ', array_slice(explode(' ', str_replace('"', "'", strip_tags($this->content))), 0, 15)), ", \t\n\r\0\x0B") . '...';
    }

    public function getTitle()
    {
        return $this->entry ? $this->entry[1] : '';
    }

    public function getTagsString()
    {
        return $this->entry ? implode('</span>, <span class="tag">', $this->entry[3]) : '';
    }

    public function getDate($format = 'H:i, j.m.y')
    {
        return date($format, strtotime($this->entry[2]));
    }

    static function isOK($a)
    {
        return isset($a) && $a && $a->entry;
    }

    static function tlit($text, $jotFlag = true)
    {
        $jot = $jotFlag ? ['j','J'] : ['й','Й'];

        $map =
        [
            'a'=> 'а', 'A'=> 'А',
            'b'=> 'б', 'B'=> 'Б',
            'c'=> 'ц', 'C'=> 'Ц',
            'č'=> 'ч', 'Č'=> 'Ч',
            'd'=> 'д', 'D'=> 'Д',
            'e'=> 'е', 'E'=> 'Е',
            'ě'=> 'е', 'Ě'=> 'Е',
            'ę'=> 'я', 'Ę'=> 'Я',
            'f'=> 'ф', 'F'=> 'Ф',
            'g'=> 'г', 'G'=> 'Г',
            'h'=> 'х', 'H'=> 'Х',
            'i'=> 'и', 'I'=> 'И',
            'j'=> $jot[0], 'J'=> $jot[1],
            'k'=> 'к', 'K'=> 'К',
            'l'=> 'л', 'L'=> 'Л',
            'm'=> 'м', 'M'=> 'М',
            'n'=> 'н', 'N'=> 'Н',
            'o'=> 'о', 'O'=> 'О',
            'p'=> 'п', 'P'=> 'П',
            'r'=> 'р', 'R'=> 'Р',
            's'=> 'с', 'S'=> 'С',
            'š'=> 'ш', 'Š'=> 'Ш',
            't'=> 'т', 'T'=> 'Т',
            'u'=> 'у', 'U'=> 'У',
            'ų'=> 'у', 'Ų'=> 'У',
            'v'=> 'в', 'V'=> 'В',
            'y'=> 'ы', 'Y'=> 'Ы',
            'z'=> 'з', 'Z'=> 'З',
            'ž'=> 'ж', 'Ž'=> 'Ж'
        ];
        
        $hardCons = ['б','в','г','д','з','к','л','н','п','р','с','т','ф','х'];

        $sym = preg_split('/(?!^)(?=.)/u', $text);

        for ($i = 0; $i < count($sym); $i++)
        {
            if (isset($map[$sym[$i]])) $sym[$i] = $map[$sym[$i]];
            if (!$i) continue;

            if ($sym[$i-1] == $jot[0] && $sym[$i] == 'у') { $sym[$i-1] = 'ю'; $sym[$i] = ''; }
            if ($sym[$i-1] == $jot[0] && $sym[$i] == 'е') { $sym[$i-1] = 'е'; $sym[$i] = ''; }
            if ($sym[$i-1] == $jot[0] && $sym[$i] == 'а') { $sym[$i-1] = 'я'; $sym[$i] = ''; }
            if ($sym[$i-1] == $jot[0] && $sym[$i] == 'я') { $sym[$i-1] = 'я'; $sym[$i] = ''; }
            if ($sym[$i-1] == 'ш' && $sym[$i] == 'ч') { $sym[$i-1] = 'щ'; $sym[$i] = ''; }

            if ($sym[$i-1] == 'ť') { $sym[$i-1] = 'т'; array_splice($sym, $i, 0, 'ь'); }
            if ($sym[$i-1] == 'ď') { $sym[$i-1] = 'д'; array_splice($sym, $i, 0, 'ь'); }
            if ($sym[$i-1] == 'ń') { $sym[$i-1] = 'н'; array_splice($sym, $i, 0, 'ь'); }
            if ($sym[$i-1] == 'ľ') { $sym[$i-1] = 'л'; array_splice($sym, $i, 0, 'ь'); }

            if ($sym[$i-1] && in_array(strtolower($sym[$i-1]), $hardCons) && $sym[$i] == $jot[0]) { $sym[$i] = 'ь'; }
            if ($sym[$i-1] == 'ь' && $sym[$i] == 'е') { $sym[$i-1] = 'е'; $sym[$i] = ''; }
        }

        return implode('', $sym);
    }
}
