<?php

class Sys
{
    static function exec($cmd)
    {
        $descriptors = array
        (
            0 => ['pipe', 'r'],
            1 => ['pipe', 'w'],
            2 => ['pipe', 'w'],
        );
        $env = array
        (
            'PATH'      => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
            'LC_ALL'    => 'en_US.UTF-8',
            'LANG'      => 'en_US.UTF-8',
        );

        flush();
        $log = '';
        $process = proc_open($cmd, $descriptors, $pipes, realpath('./'), $env);

        if (is_resource($process))
        {
            while ($s = fgets($pipes[1]))
            {
                // $s contains output
                $log .= $s;
                flush();
            }
        }

        proc_close($process);

        return $log;
    }
}